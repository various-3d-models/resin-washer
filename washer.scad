include <BOSL2/std.scad>
use <common_params.scad>

$fa = 0.5;
$fs = 0.5;

rotor_d = 80;
rotor_h = 8;
rotor_opening_d = rotor_d + 5;

motor_mount_h = 9;
table_size = [160, 110, 5];
motor_block_size = [46.25, 24.1, 23.1];

leg_a = 10;

module motor_mount(anchor = CENTER, spin = 0, orient = UP) {
    d = 5.5;
    h = motor_mount_h;

    attachable(anchor, spin, orient, d = d, l = h) {
        up(h / 2)
        zflip()
        linear_extrude(h)
        intersection() {
            circle(d = d);
            rect([3.75, 10]);
        }
        children();
    }
}

module magnets(anchor = CENTER, spin = 0, orient = UP) {
    size = [20, 10, 3];
    a = 75;

    attachable(anchor, spin, orient, size = [a, size.y, size.z]) {
        xflip_copy()
        left(a / 2)
        cuboid(size, anchor = LEFT);
        children();
    }
}

module rotor(anchor = CENTER, spin = 0, orient = UP) {
    screw_head_h = 1.5;
    screw_depth = 1.5;

    anchors = [named_anchor("mount", [0, 0, rotor_h / 2 - (screw_head_h + screw_depth)], DOWN, 0)];

    attachable(anchor, spin, orient, d = rotor_d, l = rotor_h, anchors = anchors) {
        tag_intersect("rotor") {
            tag_diff("aa")
            cyl(h = rotor_h, d = rotor_d) {
                tag("remove") {
                    position(TOP) magnets(anchor = TOP);
                    position(TOP) cyl(h = screw_head_h, d = 5, anchor = TOP) {
                        position(BOTTOM) cyl(h = screw_depth, d = 2.5, anchor = TOP) {
                            position(BOTTOM) motor_mount(anchor = TOP);
                        }
                    }
                }
            }
            tag("intersect") cross();
        }
        children();
    }
}

module motorblock(cutter = false, tagg = "motorblock", anchor = CENTER, spin = 0, orient = UP) {
    size = motor_block_size;

    motor_center = move(v_mul(LEFT + TOP, size / 2)) * right(4 + 16 / 2);
    holes_center = motor_center * down(4);
    top = motor_center * up(motor_mount_h);

    anchors = [
        named_anchor("top_mount", apply(top, [0, 0, 0]), UP, 0),
        named_anchor("motor_center", apply(motor_center, [0, 0, 0]), DOWN, 0)
    ];

    attachable(anchor, spin, orient, size = size, anchors = anchors) {
        tag_diff(tagg) {
            cuboid(size);
            multmatrix(motor_center) motor_mount(anchor = BOTTOM);
            if (cutter) {
                multmatrix(holes_center) line_copies(n = 2, l = 16) cyl(h = 50, d = 4, orient = FRONT);
            } else {
                tag("remove") multmatrix(holes_center) line_copies(n = 2, l = 16) cyl(h = 50, d = 4, orient = FRONT);
            }
        }
        children();
    }
}

module cross() {
    zrot_copies([0, 90])
    cuboid([100, cross_arm_width(), 50]);
}

module table() {
    size = table_size;

    diff(remove = "remove")
    cuboid(size) {
        tag("remove") cyl(h = size.z + 0.2, d = rotor_opening_d);
        hide("rotor motorblock") down(2) position(TOP) rotor(anchor = TOP) {
            attach("mount", "top_mount") motorblock() {
                attach("motor_center", "xxx") holder();
            }
            force_tag("remove") attach("mount", "top_mount") motorblock(cutter = true);
        }
        xflip_copy() yflip_copy() position(TOP + FRONT + LEFT) leg(anchor = TOP);
    }
}

module holder(anchor = CENTER, spin = 0, orient = UP) {
    h = 10;
    anchors = [named_anchor("xxx", [0, 0, -h], UP, 0)];

    attachable(anchor, spin, orient, size = [100, 100, 100], anchors = anchors) {
        yflip_copy()
        fwd(motor_block_size.y / 2)
        xrot(90)
        linear_extrude(3)
        difference() {
            rect([rotor_opening_d + 20, 20], anchor = TOP);
            trapezoid(h = h, w1 = rotor_opening_d, w2 = rotor_opening_d + 10, anchor = TOP);
        }
        children();
    }
}

module leg(h = 30, anchor = CENTER, spin = 0, orient = UP) {
    attachable(anchor, spin, orient, size = [1, 1, h]) {
        down(h / 2)
        linear_extrude(h)
        right_triangle(leg_a);
        children();
    }
}

module leg_extender() {
    difference() {
        linear_extrude(60)
        offset(2)
        right_triangle(leg_a);

        linear_extrude(10) right_triangle(leg_a + 0.2);
    }
}

// magnets() show_anchors();
// motor_mount() show_anchors();
// rotor() show_anchors(std = false);
// motorblock() show_anchors(std = false);
// table();
// holder() show_anchors(std=false);
// leg();
// leg_extender();
