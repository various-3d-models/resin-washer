function layer_h() = 0.2;
function magnet_dia() = 14;
function magnet_h() = 5;
function inter_magnet_l() = 50; // Center to center distance
function cross_arm_width() = 18;
