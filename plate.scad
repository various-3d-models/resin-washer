include <BOSL2/std.scad>
use <common_params.scad>

$fa = 0.5;
$fs = 0.5;

module plate() {
    d = 120;

    tag_diff("plate")
    cyl(d = d, h = 2) {
        position(BOTTOM) {
            cyl(d = 60, h = 8 + 5, anchor = TOP);
            tag("remove") down(5) zrot_copies([0, 90]) cuboid([100, cross_arm_width() + 0.2, 100], anchor = TOP);
        }
    }
}

plate();
