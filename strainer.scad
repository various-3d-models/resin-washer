include <BOSL2/rounding.scad>
include <BOSL2/std.scad>
use <ikea.scad>
use <utils.scad>

$fa = 0.5;
$fs = 0.5;

bottom2base = 14;
nub2nub = 100;

base_outline_region = container4_2l_slice(bottom2base);
base_size = concat(my_size(base_outline_region[0]), 6);
container_top_size = my_size(container4_2l_slice(220 - 0.1)[0]);

grid_width = 1.5;
grid_spacing = 10;
bearing_hole_d = 22;

handle_size = [12, base_size.y, 200];
nub_h = 8;
nub_d = 8;
nub_cutout_length = 30;
nub2bottom = bottom2base + base_size.z / 2;

outline_w = 2;

container_lip_w = 10;
container_top_to_locked_h = 50;
a = (container_top_size.x - nub2nub) / 2;
notch_h = sqrt(container_top_to_locked_h ^ 2 + a ^ 2);
alfa = atan(a / container_top_to_locked_h);

module outline() {
    tag_diff("outline")
    region(base_outline_region) {
        tag("remove") yflip_copy() xcopies(spacing = nub2nub, n = 2) position(FRONT) rect([nub_cutout_length, nub_h], anchor = FRONT);
    }
}

module innerOutline() {
    offset(r = -outline_w) outline();
}

module outlineBorder(h, center = false) {
    linear_extrude(h, center = center)
    difference() {
        outline();
        innerOutline();
    }
}

module grid() {
    intersection() {
        zrot_copies([0, 90])
        ycopies(spacing = grid_spacing, l = 200)
        rect([200, grid_width]);

        difference() {
            innerOutline();
            circle(d = bearing_hole_d);
        }
    }
}

module nub() {
    intersection() {
        tag_diff("nub")
        cyl(h = nub_h, d = nub_d, anchor = BOTTOM) {
            attach(TOP, TOP) zflip() cyl(h = 1, d = nub_d + 1);
            tag("remove") up(2) attach(BOTTOM, TOP) zflip() cuboid([2, 20, 100]);
        }
        cuboid([100, base_size.z, 100]);
    }
}

module base(anchor = CENTER, spin = 0, orient = UP) {
    size = base_size;
    anchors = [named_anchor("aaa", [-nub2nub / 2, -size.y / 2 + nub_h, 0], FRONT)];

    attachable(anchor, spin, orient, size = size, anchors = anchors) {
        position(BOTTOM) outlineBorder(size.z + 3);
        children();
    }
}

module strainer(anchor = CENTER, spin = 0, orient = UP, withHandle = false, open = false) {
    base() {
        xflip_copy() yflip_copy() attach("aaa", BOTTOM) nub();
        position(BOTTOM) linear_extrude(3) grid();

        tag_diff("bearing_hole")
        position(BOTTOM) cyl(h = 7.6, d = bearing_hole_d + 2 * outline_w, anchor = BOTTOM) tag("remove") {
            position(TOP) cyl(h = 7, d = bearing_hole_d, anchor = TOP);
            cyl(h = 100, d = bearing_hole_d - 3);
        }

        if (withHandle) {
            xflip_copy() attach("aaa", "aaa") zrot(open ? alfa : 0) handle();
        }
    }
}

module container(anchor = CENTER, spin = 0, orient = UP) {
    vnf_polyhedron(container4_2l(), anchor = anchor, spin = spin, orient = orient) children();
}

module handle(anchor = CENTER, spin = 0, orient = UP) {
    size = handle_size;
    handle_w = nub_h;

    module notch() {
        h = handle_w;

        rot([90, 0, 0]) {
            cuboid([12, 12, h], anchor = BOTTOM + RIGHT + FRONT, rounding = 5, edges = [LEFT + BACK, LEFT + FRONT]);
            tag("remove") zrot(-alfa) cuboid([container_lip_w + 1, 50, h], anchor = BACK + BOTTOM + RIGHT);
        }
    }

    module handle_base(anchor = CENTER, spin = 0, orient = UP) {
        module base() rect([size.y, size.z], rounding = [20, 20, 0, 0]);

        attachable(anchor, spin, orient, size = size) {
            rot([90, 0, 90])
            linear_extrude(size.x, center = true)
            difference() {
                base();
                offset(r = -handle_w) base();
                fwd(10) offset(r = -handle_w) base();
            }
            children();
        }
    }

    anchors = [named_anchor("aaa", [0, -size.y / 2 + nub_h, -size.z / 2 + nub2bottom], BACK, 180)];

    attachable(anchor, spin, orient, size = size, anchors = anchors) {
        diff()
        intersect()
        handle_base() {
            tag("remove") yflip_copy() up(nub2bottom) position(BOTTOM + FRONT)
            xrot(90) cyl(h = nub_h + 10, d = nub_d + 0.5, anchor = TOP) {
                attach(TOP, TOP) zflip() cyl(h = 1 + 0.5, d = nub_d + 1 + 0.5);
            }
            yflip_copy() {
                up(nub2bottom + notch_h) position(BOTTOM + BACK) notch();
                up(bottom2base) position(FRONT + BOTTOM + LEFT) cuboid([50, 20, 50], anchor = FRONT + RIGHT + TOP);
            }
            tag("intersect") right(nub2nub / 2) position(BOTTOM) container(anchor = BOTTOM);
        }

        children();
    }
}

// strainer();
// handle();
strainer(withHandle = true, open = false);
