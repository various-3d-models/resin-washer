include <BOSL2/std.scad>

function my_vnf_cut(vnf, plane) =
let(cut_bnd = vnf_halfspace(plane, vnf, boundary = true),
    cutvnf = cut_bnd[0],
    boundary = [for (b = cut_bnd[1]) select(cutvnf[0], b)],
    region = [for (path = boundary)[for (pnt = path) project_plane(plane, pnt)]])
region;

function my_size(pts) =
let(bounds = pointlist_bounds(pts)) bounds[1] - bounds[0];
