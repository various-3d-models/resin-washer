include <BOSL2/rounding.scad>
include <BOSL2/std.scad>
use <utils.scad>

container_h = 220;
container_top_size = [190, 130];
container_bottom_size = [160, 100];
container_side_r = 14;
container_bottom_r = 9;

function container4_2l() =
rounded_prism(rect(container_bottom_size), rect(container_top_size), h = container_h,
              joint_bot = container_bottom_r, joint_sides = container_side_r, k = 1);

function container4_2l_slice(height_from_bottom) =
my_vnf_cut(container4_2l(), plane_from_normal([0, 0, 1], [0, 0, -container_h / 2 + height_from_bottom]));
