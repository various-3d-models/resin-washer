include <BOSL2/std.scad>
use <common_params.scad>

$fa = 0.5;
$fs = 0.5;

magnet_dia = magnet_dia() + 0.5;
magnet_h = magnet_h() + 1*layer_h();
rotor_dia = 75;
h_up_to_top_of_vanes = 9;
h_up_to_bottom_of_bearing = 11;

module rotor() {
    module cutter(s=100) {
        tag_scope("cutter")
        diff()
        cuboid(s) {
            tag("remove") {
                cyl(h=s + 1, d=25, anchor=CENTER);
                zrot_copies([0:90:359])
                pie_slice(h=s + 1, d=rotor_dia, ang=45, spin=-45/2, anchor=CENTER);
            }
        }
    }

    diff()
    cyl(d=rotor_dia, h=magnet_h + 8*layer_h()) {
        tag("remove") {
            cutter();
            // Magnets
            xflip_copy()
            left(inter_magnet_l()/2) up(4*layer_h())
            position(BOTTOM) cyl(d=magnet_dia, h=magnet_h, anchor=BOTTOM);
        }

        // Vanes
        zrot_copies([0, 90])
        position(BOTTOM) cuboid([100, 4, h_up_to_top_of_vanes], anchor=BOTTOM);

        // Mounting column
        position(BOTTOM) cyl(d=10, h=h_up_to_bottom_of_bearing, anchor=BOTTOM) {
            position(TOP) cyl(d=8, h=7, anchor=BOTTOM) {
                tag("remove") position(TOP) cyl(d=2.5, h=12, anchor=TOP);
            }
        }
    }
}

module washer() {
    linear_extrude(2)
    difference() {
        circle(d = 11);
        circle(d = 3);
    }
}

rotor();
// washer();
